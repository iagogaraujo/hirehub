<!-- README -->

# Hirehub

Projeto desenvolvido para a disciplina de Projeto do curso de Análise e Desenvolvimento de Sistemas do Instituto Federal de Educação, Ciência e Tecnologia de São Paulo - Campus Jacareí.

## Descrição

O projeto consiste em um sistema de gerenciamento de contratação de serviços de profissionais autônomos, onde o cliente pode contratar um profissional para realizar um serviço e o profissional pode se cadastrar para oferecer seus serviços.

## Tecnologias

- [x] React Native (Front-end)
- [x] Expo (Front-end)
- [x] Node.js (Back-end)
- [x] Express (Back-end)
- [x] MongoDB (Banco de dados)
- [x] Mongoose (Comunicação com o banco de dados)
- [x] JWT (Autenticação)
- [x] Bcrypt (Criptografia de senhas)

## Instalação

Para instalar o projeto, é necessário ter o Node.js e o MongoDB instalados em sua máquina.
Alterações devem ser feitas no arquivo .env, que se encontra na pasta backend, para que o projeto funcione corretamente.

### Backend

Para instalar o backend, basta entrar na pasta backend e executar o comando:

```sh
yarn
```

ou

```sh
npm install
```

Para iniciar o backend, basta executar o comando:

```sh
yarn start
```

ou

```sh
npm start
```

### Frontend

Para instalar o frontend, basta entrar na pasta frontend e executar o comando:

```sh
yarn
```

ou

```sh
npm install
```

Para iniciar o frontend, basta executar o comando:

```sh
yarn start
```

ou

```sh
npm start
```

## Contribuidores

Este projeto foi desenvolvido por: **Iago Araújo**
