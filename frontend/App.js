import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

/* Screens */
import OnboardScreen from './src/screens/onboard/OnboardScreen';
import LoginScreen from './src/screens/auth/login/LoginScreen';
import IntroductionScreen from './src/screens/auth/introduction/IntroductionScreen';
import RegisterScreen from './src/screens/auth/register/RegisterScreen';
import TempPage from './src/screens/TempPage';
import HomeScreen from './src/screens/main/home/HomeScreen';
// import HomeScreen from './src/screens/main/home/HomeScreen';

/* Native Base */
import { NativeBaseProvider, Box } from 'native-base';
import SearchScreen from './src/screens/main/search/SearchScreen';
import ProfileScreen from './src/screens/main/profile/ProfileScreen';
import AddScreen from './src/screens/main/add/AddScreen';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NativeBaseProvider>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Onboard"
            component={SearchScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Introduction"
            component={IntroductionScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Register"
            component={RegisterScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="TempPage"
            component={TempPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Search"
            component={SearchScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Add"
            component={AddScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Profile"
            component={ProfileScreen}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </NativeBaseProvider>
  );
}

export default App;
