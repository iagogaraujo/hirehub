import { Box, FormControl, Input, Text } from 'native-base';
import React, { Component } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import api from '../../../api/helper';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import styles from './styles';

export default class AddScreen extends Component {
  /*
        SERVIÇO
            - Título (name)
            - Descrição (description)
            - Imagem (image) - padrão temporario (https://picsum.photos/200/300)
            - Preço (price)
            - Categoria (category)
            - Localização (location)
            - Contato [A FAZER]          
  */

  state = {
    name: '',
    description: '',
    price: '',
    image: 'https://picsum.photos/200/300',
    category: '',
    location: '',
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  // Função para adicionar um serviço
  handleSubmit = () => {
    api
      .post('/api/service/create-service', {
        ...this.state,
      })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    // Entrada de dados
    const inputFields = [
      {
        Titulo: 'name',
        Descrição: 'description',
        Preço: 'price',
        Categoria: 'category',
        Preço: 'price',
        Localização: 'location',
      },
    ];
    return (
      <SafeAreaView>
        <ScrollView
          style={{
            height: '100%',
            padding: 20,
          }}
        >
          <Box style={styles.rootContainer}>
            <Box style={styles.titleContainer}>
              <Text fontSize="3xl">Adicione o seu serviço!</Text>
            </Box>
            <Box style={styles.imageRow}>
              <Box style={styles.imageContainer}>
                <Image
                  style={styles.image}
                  source={{ uri: 'https://picsum.photos/200/300' }}
                />
              </Box>
            </Box>
            <Box style={styles.formContainer}>
              {inputFields.map((field) => {
                return Object.keys(field).map((key) => {
                  return (
                    <FormControl key={key}>
                      <FormControl.Label
                        _text={{
                          fontSize: 'sm',
                          color: 'muted.700',
                          fontWeight: 600,
                        }}
                      >
                        {key}
                      </FormControl.Label>
                      <Input
                        onChangeText={(text) =>
                          this.handleChange(field[key], text)
                        }
                      />
                    </FormControl>
                  );
                });
              })}
              <TouchableOpacity
                onPress={this.handleSubmit}
                style={[
                  styles.button,
                  StyleSheet.create({
                    marginTop: 20,
                  }),
                ]}
              >
                <Text style={styles.buttonText}>Adicionar</Text>
              </TouchableOpacity>
            </Box>
          </Box>
          {/* Para evitar o footbar */}
          <View style={{ height: 100 }}></View>
        </ScrollView>
        <CustomFooter navigation={this.props.navigation} />
      </SafeAreaView>
    );
  }
}
