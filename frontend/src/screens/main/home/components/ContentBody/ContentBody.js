import React, { Component } from 'react';
import { Text, View } from 'react-native';
import colors from '../../../../../assets/styles/index';
import BigCards from './components/BigCards/BigCards';
import Categories from './components/Categories/Categories';
import CustomCarousel from './components/CustomCarousel/CustomCarousel';
import OtherServices from './components/OtherServices/OtherServices';
import ServiceAsk from './components/ServiceAsk/ServiceAsk';
import styles from './styles';

export default class ContentBody extends Component {
  // Quando o componente for montado, forçar a atualização da página
  async componentDidMount() {
    // TODO: Implementar
  }

  render() {
    return (
      <View style={styles.section}>
        <View>
          <View style={styles.sectionTitle}>
            <Text style={styles.sectionTitleText}>
              Última categoria acessada
            </Text>
          </View>
          <View style={styles.bigCardContainer}>
            <BigCards />
          </View>
        </View>
        <View>
          <View style={styles.sectionTitle}>
            <Text style={styles.sectionTitleText}>
              Serviços próximos a você
            </Text>
          </View>
          <View style={styles.carouselContainer}>
            <CustomCarousel />
          </View>
        </View>
        <View>
          <View style={styles.sectionTitle}>
            <Text style={styles.sectionTitleText}>Outros serviços</Text>
          </View>
          <View style={styles.otherServicesContainer}>
            <OtherServices />
          </View>
        </View>

        <View>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                backgroundColor: colors.PRIMARY_COLOR,
                height: 1,
                flex: 1,
                marginLeft: 10,
                alignSelf: 'center',
              }}
            />
            <Text
              style={{
                alignSelf: 'center',
                paddingHorizontal: 5,
                fontSize: 16,
                color: colors.PRIMARY_COLOR,
              }}
            >
              Categorias
            </Text>
            <View
              style={{
                backgroundColor: colors.PRIMARY_COLOR,
                height: 1,
                flex: 1,
                marginRight: 10,
                alignSelf: 'center',
              }}
            />
          </View>

          <Categories />
        </View>

        <View>
          <View style={styles.sectionTitle}>
            <Text style={styles.sectionTitleText}>
              Não achou o que procurava?
            </Text>
          </View>
          <View style={styles.inputAskService}>
            <ServiceAsk />
          </View>
        </View>
      </View>
    );
  }
}
