import { Box } from 'native-base';
import React from 'react';
import { ImageBackground, Text, TouchableOpacity, View } from 'react-native';

import styles from './styles';

const CustomCarouselItem = ({ title, description, price, image }) => {
  return (
    <Box borderRadius={5} shadow={5} style={styles.rootContainer}>
      <TouchableOpacity>
        <ImageBackground
          source={{ uri: image }}
          style={styles.image}
          imageStyle={{ borderRadius: 5 }}
        >
          <View style={styles.container}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{title}</Text>
            </View>
            <View style={styles.descriptionContainer}>
              <Text style={styles.description}>{description}</Text>
            </View>
            <View style={styles.priceContainer}>
              <Text style={styles.price}>R$ {price}</Text>
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </Box>
  );
};

export default CustomCarouselItem;
