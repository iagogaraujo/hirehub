import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  sectionTitle: {
    flex: 1,
    width: '100%',
    padding: 10,
  },
  text: {
    fontSize: 16,
    color: '#000',
  },
  inputAskService: {
    flex: 1,
    width: '100%',
    padding: 10,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
  },
  buttonAskService: {
    flex: 1,
    width: '100%',
    padding: 10,
  },
  button: {
    backgroundColor: '#000',
    padding: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
  },
});
