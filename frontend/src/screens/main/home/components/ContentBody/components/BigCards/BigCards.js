import { Box } from 'native-base';
import React, { Component } from 'react';
import {
  ImageBackground,
  Text,
  TouchableOpacity,
  Vibration,
} from 'react-native';
import colors from '../../../../../../../assets/styles/index';
import styles from './styles';

export default class BigCards extends Component {
  render() {
    return (
      <Box
        shadow="8"
        rounded="lg"
        backgroundColor={colors.QUINARY_COLOR}
        style={styles.rootContainer}
      >
        <TouchableOpacity
          style={{
            flex: 1,
            // flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            alignContent: 'center',
            width: '100%',
            height: '100%',
          }}
          onclick={() => {
            Vibration.vibrate(100);
          }}
        >
          <ImageBackground
            source={{
              uri: 'https://images.unsplash.com/photo-1530124566582-a618bc2615dc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80',
            }}
            alt="Alternate Text"
            resizeMode="cover"
            style={styles.backgroundImage}
            imageStyle={{ borderRadius: 9 }}
          >
            <Text style={styles.cardText}>Mecânica</Text>
          </ImageBackground>
        </TouchableOpacity>
      </Box>
    );
  }
}
