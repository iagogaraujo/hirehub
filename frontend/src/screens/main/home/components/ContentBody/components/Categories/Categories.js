import { Box } from 'native-base';
import React from 'react';
import { ImageBackground, Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';

const mockData = [
  {
    id: 1,
    title: 'Casa',
    image: 'picsum.photos/200/300',
  },
  {
    id: 2,
    title: 'Casa',
    image: 'picsum.photos/200/300',
  },
  {
    id: 3,
    title: 'Casa',
    image: 'picsum.photos/200/300',
  },
  {
    id: 4,
    title: 'Casa',
    image: 'picsum.photos/200/300',
  },
  {
    id: 5,
    title: 'Casa',
    image: 'picsum.photos/200/300',
  },
  {
    id: 6,
    title: 'Casa',
    image: 'picsum.photos/200/300',
  },
];

const CategoriesItem = ({ category }) => {
  return (
    <TouchableOpacity style={styles.rootContainer}>
      <Box
        width="100%"
        height="100%"
        justifyContent="center"
        alignItems="center"
        borderRadius="lg"
        overflow="hidden"
      >
        <ImageBackground
          source={{ uri: `https://${category.image}` }}
          style={styles.backgroundImage}
        >
          <Text style={styles.cardText}>{category.title}</Text>
        </ImageBackground>
      </Box>
    </TouchableOpacity>
  );
};

const Categories = () => {
  return (
    <View style={styles.section}>
      <View style={styles.bigCardContainer}>
        {mockData.map((category) => (
          <CategoriesItem key={category.id} category={category} />
        ))}
      </View>
    </View>
  );
};

export default Categories;
