import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  rootContainer: {
    backgroundColor: 'white',
    width: 200,
    height: 200,
    margin: 10,
  },
  image: {
    width: '100%',
    height: '100%',
    justifyContent: 'flex-end',
  },
  titleContainer: {
    padding: 5,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  title: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  descriptionContainer: {
    padding: 5,
  },
  description: {
    color: 'white',
    fontSize: 12,
  },
  priceContainer: {
    padding: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  price: {
    color: 'white',
    fontSize: 12,
  },
  container: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderRadius: 5,
  },
});
