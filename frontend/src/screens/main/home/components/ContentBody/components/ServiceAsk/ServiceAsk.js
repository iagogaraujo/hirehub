import { Entypo } from '@expo/vector-icons';
import { Icon } from 'native-base';
import React from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';

import styles from './styles';

const ServiceAsk = () => {
  return (
    <View style={styles.container}>
      <View style={styles.inputAskService}>
        <TextInput
          style={styles.input}
          placeholder="Digite aqui o serviço que deseja"
          onPressIn={() => {
            // TODO: Move to the bottom of the page
          }}
        />
      </View>
      <View style={styles.buttonAskService}>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Enviar</Text>
          <Icon as={Entypo} name="chevron-right" size="sm" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ServiceAsk;
