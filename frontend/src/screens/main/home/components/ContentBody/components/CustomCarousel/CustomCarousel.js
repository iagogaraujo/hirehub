import { FlatList } from 'native-base';
import React, { Component } from 'react';
import { View } from 'react-native';
import api from '../../../../../../../api/helper';
import CustomCarouselItem from './CustomCarouselItem/CustomCarouselItem';
import styles from './styles';

// Função para obter os serviços
const getServices = async () => {
  try {
    const res = await api.get('/api/service/get-all');

    if (res.data.success) {
      return res.data;
    } else {
      return { error: res.data.error };
    }
  } catch (err) {
    return { error: err };
  }
};

export default class CustomCarousel extends Component {
  constructor() {
    super();
    this.state = {
      services: {},
    };
  }

  async componentDidMount() {
    const services = await getServices();
    this.setState({ services });
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.services.services}
          renderItem={({ item }) => (
            <CustomCarouselItem
              title={item.name}
              description={item.description}
              price={item.price}
              image={item.image}
            />
          )}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }
}
