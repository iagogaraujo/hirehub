import { Entypo } from '@expo/vector-icons';
import { Icon, IconButton, ScrollView } from 'native-base';
import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';

import styles from './styles';

// Mudar para dados reais posteriormente
const mockData = [
  {
    title: 'Corte de Cabelo',
    price: 20,
    popularity: 4,
    favorite: true,
    image:
      'https://images.unsplash.com/photo-1503951914875-452162b0f3f1?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
  },
  {
    title: 'Pintura',
    price: 50,
    popularity: 3,
    favorite: false,
    image:
      'https://images.unsplash.com/photo-1562259949-e8e7689d7828?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1131&q=80',
  },
  {
    title: 'Mecânica',
    price: 100,
    popularity: 5,
    favorite: true,
    image:
      'https://images.unsplash.com/photo-1619642751034-765dfdf7c58e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80',
  },
  {
    title: 'Limpeza',
    price: 50,
    popularity: 2,
    favorite: false,
    image:
      'https://images.unsplash.com/photo-1502005097973-6a7082348e28?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80',
  },
];

const OtherServicesItem = ({ item }) => {
  return (
    <TouchableOpacity>
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={{ uri: item.image }} />
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.price}>R$ {item.price}</Text>
        </View>
        <View style={styles.favoriteContainer}>
          <IconButton
            icon={
              item.favorite ? (
                <Icon as={Entypo} name="heart" />
              ) : (
                <Icon as={Entypo} name="heart-outlined" />
              )
            }
            onPress={() => {}}
            colorScheme={item.favorite ? 'red' : 'gray'}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const OtherServices = () => {
  return (
    <View style={styles.rootContainer}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        nestedScrollEnabled
        style={styles.scrollView}
      >
        {mockData.map((item) => (
          <OtherServicesItem key={item.title} item={item} />
        ))}
      </ScrollView>
    </View>
  );
};

export default OtherServices;
