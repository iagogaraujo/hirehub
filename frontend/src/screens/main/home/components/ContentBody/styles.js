import { StyleSheet } from 'react-native';
import colors from '../../../../../assets/styles/index';

export default StyleSheet.create({
  sectionTitleText: {
    color: colors.PRIMARY_COLOR,
    fontWeight: 'bold',
    paddingBottom: 10,
    paddingLeft: 10,
  },
  bigCardContainer: {
    height: 175,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
