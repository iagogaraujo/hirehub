import React from 'react';
import { View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from './styles';

import { Text } from 'native-base';
import { SafeAreaView } from 'react-native-safe-area-context';
import LinearText from './LinearText/LinearText';

export class CustomHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: 'DEFAULT',
    };
  }

  async componentDidMount() {
    this.state.user = await AsyncStorage.getItem('username');
    this.forceUpdate();
  }

  render() {
    return (
      <SafeAreaView>
        <View style={styles.rootContainer}>
          <View style={styles.welcomeContainer}>
            <Text fontSize="3xl">Bem vindo, </Text>
            <LinearText text={JSON.stringify(this.state.user)} />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

export default CustomHeader;
