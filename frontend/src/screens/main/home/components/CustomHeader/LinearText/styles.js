import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  rootContainer: {
    flex: 1,
    flexDirection: 'row',
    height: 50,
    width: 200,
  },
  linearGradient: {
    flex: 1,
  },
});
