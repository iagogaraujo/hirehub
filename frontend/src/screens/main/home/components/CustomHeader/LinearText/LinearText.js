import MaskedView from '@react-native-community/masked-view';
import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { Text } from 'native-base';
import styles from './styles';

const LinearText = ({ text }) => {
  return (
    <MaskedView
      style={styles.rootContainer}
      maskElement={
        <Text fontSize="3xl" fontWeight="bold">
          {text}
        </Text>
      }
    >
      <LinearGradient
        colors={['#4c669f', '#3b5998', '#192f6a']}
        start={{ x: 0, y: 0 }}
        end={{ x: 0.5, y: 0 }}
        style={styles.linearGradient}
      />
    </MaskedView>
  );
};

export default LinearText;
