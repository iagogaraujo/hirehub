import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  rootContainer: {
    height: 100,
    alignContent: 'center',
    justifyContent: 'center',
  },
  welcomeContainer: {
    marginLeft: 20,
    flexDirection: 'row',
  },
});
