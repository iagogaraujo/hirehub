import { ScrollView } from 'native-base';
import React, { Component } from 'react';
import { View } from 'react-native';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import ContentBody from './components/ContentBody/ContentBody';
import { CustomHeader } from './components/CustomHeader/CustomHeader';

export default class HomeScreen extends Component {
  render() {
    return (
      <View>
        <ScrollView>
          <CustomHeader />
          <View>
            <ContentBody />
            <View style={{ height: 100 }} />
          </View>
        </ScrollView>
        <CustomFooter navigation={this.props.navigation} />
      </View>
    );
  }
}
