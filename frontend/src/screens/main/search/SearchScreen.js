import { Entypo } from '@expo/vector-icons';
import { Box, Icon, Text } from 'native-base';
import React, { Component } from 'react';
import {
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import styles from './styles';
import api from '../../../api/helper';

const mockData = [{
  id: 1,
  title: 'Corte de cabelo',
  description: 'Corte de cabelo masculino',
  imageUri: 'https://picsum.photos/200',
}
];


export default class SearchScreen extends Component {

  ResultItem = ({ title, description, imageUri }) => {
    return (
      <TouchableOpacity>
        <View style={[styles.resultItem, styles.shadowBox]}>
          <Image style={styles.resultImage} source={{ uri: imageUri }} />
          <View style={styles.resultText}>
            <Text style={styles.resultTitle}>{title}</Text>
            <Text style={styles.resultDescription}>{description}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  SearchBar = () => {
    return (
      <View style={styles.container}>
        <View style={styles.inputSearchContainer}>
          <TextInput
            style={styles.input}
            placeholder="Digite aqui o serviço que deseja"
            onChangeText={(text) => {
              this.setState({ searchText: text });
            }}
          />
        </View>
        <View style={styles.buttonSearchContainer}>
          <TouchableOpacity
            onPress={async () => {
              var serviceResult = await this.getData(this.state.searchText);
              var {_id, name, description, image} = serviceResult;
              console.log(name);
              this.setState({ services: [{id: _id, title: name, description: description, imageUri: image}] });
            }}
          style={styles.button}>
            <Text style={styles.buttonText}>Buscar</Text>
            <Icon as={Entypo} name="chevron-right" size="sm" />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  getData = async (serviceName) => {
    const response = await api.post('/api/service/get-by-name', {
      name: serviceName,
    });

    const { description, image, name } = response.data.services[0];

    return response.data.services[0];

    // return response.data;
    // console.log(response.data)
  };

  constructor(props) {
    super(props);
    this.state = {
      services: [],
      searchText: '',
    };
  }

  render() {
    return (
      <View>
        <SafeAreaView>
          <ScrollView
            style={{
              height: '100%',
              padding: 20,
            }}
          >
            <Box style={styles.rootContainer}>
              <Box style={styles.titleContainer}>
                <Text fontSize="3xl">Busque por serviços</Text>
              </Box>
              <Box style={styles.searchContainer}>
                <this.SearchBar />
              </Box>
              <Box style={styles.resultsContainer}>
                {
                  this.state.services.map((service) => {
                    return (
                      <this.ResultItem
                        key={service.id}
                        title={service.title}
                        description={service.description}
                        imageUri={service.imageUri}
                      />
                    );
                  })
                }
              </Box>
            </Box>
          </ScrollView>
          <CustomFooter navigation={this.props.navigation} />
        </SafeAreaView>
      </View>
    );
  }
}
