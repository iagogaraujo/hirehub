import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import React, { Component } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Box, ScrollView } from 'native-base';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import AsyncStorage from '@react-native-async-storage/async-storage';
import colors from '../../../assets/styles/';

export default class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: 'DEFAULT',
    };
  }

  async componentDidMount() {
    this.state.user = await AsyncStorage.getItem('username');
    this.forceUpdate();
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView
          style={styles.rootContainer}
          contentContainerStyle={{
            flexGrow: 1,
            height: '100%',
          }}
        >
          {/* Profile icon */}
          <Box style={styles.profileContainer}>
            <Box style={styles.profileIconContainer}>
              <Image
                style={styles.profileIcon}
                source={{
                  uri: 'https://picsum.photos/200',
                }}
              />
            </Box>
            <Box style={styles.profileNameContainer}>
              <Text style={styles.profileName}>
                {JSON.stringify(this.state.user)}
              </Text>
            </Box>
          </Box>
          {/* Descrição */}
          <Box style={styles.descriptionContainer}>
            <Text style={styles.description}>
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
              tincidunt."
            </Text>
          </Box>

          {/* Botões */}
          <Box style={[styles.buttonsContainer, {marginTop: 15, marginBottom: 40}]}>

            <ScrollView horizontal={false} style={{width: '100%'}}>
            <TouchableOpacity style={styles.menuButton}>
              <Text style={styles.menuButtonText}>Editar perfil</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menuButton}>
              <Text style={styles.menuButtonText}>Meus serviços</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menuButton}>
              <Text style={styles.menuButtonText}>Ver contatos</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menuButton}>
              <Text style={styles.menuButtonText}>Configurações de aplicativo</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.menuButton, {backgroundColor: 'red'}]}>
              <Text style={styles.menuButtonText}>Sair do perfil</Text>
            </TouchableOpacity>
            </ScrollView>
            
          </Box>
        </ScrollView>
        <CustomFooter navigation={this.props.navigation} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  rootContainer: {
    backgroundColor: colors.background,
    padding: 20,
  },
  profileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    background
  },
  profileIconContainer: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileIcon: {
    width: 80,
    height: 80,
    borderRadius: 40,
  },
  profileNameContainer: {
    marginLeft: 20,
  },
  profileName: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  descriptionContainer: {
    marginTop: 20,
    marginHorizontal: 20,
  },
  description: {
    fontSize: 16,
    color: '#737373',
    textAlign: 'left',
  },
  buttonsContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    
  },
  button: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: colors.white,
    fontSize: 16,
  },
  menuButton: {
    width: '100%',
    backgroundColor: 'black ',
    borderRadius: 8,
    padding: 10,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  menuButtonText: {
    color: '#fff',
    fontSize: 16,
    marginRight: 10,
  },
});
