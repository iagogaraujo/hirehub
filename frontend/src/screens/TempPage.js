import { Text, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';
import { Box } from 'native-base';

export default class TempPage extends Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <Text style={styles.text}>Página de Testes</Text> */}
        <Box
          p="2"
          bg="primary.500"
          _text={{
            fontSize: 'md',
            fontWeight: 'medium',
            color: 'warmGray.50',
            letterSpacing: 'lg',
          }}
          shadow={3}
        >
          Página de Testes
        </Box>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
