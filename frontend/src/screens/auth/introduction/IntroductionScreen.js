import React, { Component } from 'react';
import { Image, View } from 'react-native';
import AuthButton from '../../../components/Auth/AuthButton';
import styles from './styles';

export default class IntroductionScreen extends Component {
  render() {
    return (
      <View style={styles.rootContainer}>
        {this._renderBanner()}
        {this._renderForm()}
      </View>
    );
  }

  _renderBanner() {
    return (
      <View style={styles.bannerContainer}>
        <Image
          source={require('../../../assets/images/logo-transparent.png')}
          resizeMode="contain"
          style={styles.banner}
        />
      </View>
    );
  }

  _renderForm() {
    return (
      <View style={styles.formContainer}>
        <AuthButton
          text="Login"
          type="primary"
          onPress={() => {
            this.props.navigation.navigate('Login');
          }}
        ></AuthButton>
        <AuthButton
          text="Registro"
          type="secondary"
          onPress={() => {
            this.props.navigation.navigate('Register');
          }}
        ></AuthButton>
      </View>
    );
  }
}
