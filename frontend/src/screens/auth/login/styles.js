import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  rootContainer: {
    backgroundColor: '#fff',
    alignItems: 'center',
    flex: 1,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  bannerContainer: {
    paddingBottom: 10,
    paddingTop: 70,
    padding: 50,
  },
  banner: {
    height: 100,
  },
  buttonRow: {
    flexDirection: 'column',
    height: 140,
    justifyContent: 'space-between',
    padding: 20,
    width: '75%',
  },
  error: {
    color: 'red',
  },
});
