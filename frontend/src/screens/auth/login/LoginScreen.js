import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import api from '../../../api/helper';
import AuthButton from '../../../components/Auth/AuthButton';
import AuthInput from '../../../components/Auth/AuthInput';
import styles from './styles';
import { isEmail } from '../../../utils/validators';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: '',
    };
  }

  // Função para lidar com o login
  handleLogin = async () => {
    const { email, password } = this.state;
    const { navigation } = this.props;

    // Verifica se os campos estão preenchidos
    if (email === '' || password === '')
      return this.setState({ error: 'Campo obrigatório*' });

    this.setState({ error: '' });

    // Verifica se o email é válido
    if (isEmail({ email })) {
      try {
        // Faz a requisição para o backend
        const res = await api.post('/api/user/sign-in', {
          ...this.state,
        });

        // Se o login for mal sucedido
        if (!res.data.success) {
          alert('Login falhou! Tente novamente.');
          return;
        }

        // Salvando o token e o username no AsyncStorage
        await AsyncStorage.setItem('token', res.data.token);
        await AsyncStorage.setItem('username', res.data.user.fullname);

        // Redireciona para a tela de Home
        navigation.navigate('Home');
      } catch (error) {
        this.handleLoginError(error);
      }
    } else {
      alert('Email inválido');
    }
  };

  // Função para lidar com os erros do login
  handleLoginError = (error) => {
    if (String(error).includes('401')) {
      return alert('Email ou senha inválidos');
    }
    if (String(error).includes('Network Error')) {
      return alert('Erro de conexão');
    }
    if (String(error).includes('timeout')) {
      return alert('Tempo de conexão excedido');
    }
    console.log(error);
    return alert('Erro interno');
  };

  render() {
    return (
      <View style={styles.rootContainer}>
        {/* Banner */}
        <View style={styles.bannerContainer}>
          <Image
            source={require('../../../assets/images/logo-transparent.png')}
            resizeMode="contain"
            style={styles.banner}
          />
        </View>
        {/* Form */}
        <View style={styles.formContainer}>
          <Text style={styles.error}>{this.state.error}</Text>
          <AuthInput
            type="envelope"
            placeholder={'Email'}
            value={this.state.email}
            onChangeText={(value) => this.setState({ email: value })}
          />
          <Text style={styles.error}>{this.state.error}</Text>
          <AuthInput
            type="lock"
            placeholder={'Senha'}
            value={this.state.password}
            onChangeText={(value) => this.setState({ password: value })}
            secureTextEntry={true}
          />
        </View>
        <View style={styles.buttonRow}>
          <AuthButton
            text="Login"
            type="primary"
            onPress={async () => {
              await this.handleLogin();
            }}
          />
          <AuthButton
            text="Voltar"
            type="secondary"
            onPress={() => {
              this.props.navigation.goBack();
            }}
          />
        </View>
      </View>
    );
  }
}
