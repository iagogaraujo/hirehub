import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  rootContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  bannerContainer: {
    padding: 50,
    paddingTop: 70,
  },
  banner: {
    height: 100,
  },
  buttonRow: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '75%',
    height: 140,
    padding: 20,
  },
  error: {
    color: 'red',
  },
});
