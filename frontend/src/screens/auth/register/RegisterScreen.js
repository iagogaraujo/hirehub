import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import api from '../../../api/helper';
import AuthButton from '../../../components/Auth/AuthButton';
import AuthInput from '../../../components/Auth/AuthInput';
import { isEmail } from '../../../utils/validators';
import styles from './styles';

export default class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullname: '',
      email: '',
      password: '',
      confirmPassword: '',
      usertype: 'user',
      error: '',
    };
  }

  register = async () => {
    const { fullname, email, password, confirmPassword, usertype } = this.state;
    if (
      fullname === '' ||
      email === '' ||
      password === '' ||
      confirmPassword === ''
    ) {
      // console.log('Campo obrigatório*');
      this.setState({ error: 'Preencha todos os campos*' });
      return;
    }

    if (password !== confirmPassword) {
      // console.log('Senhas não conferem');
      this.setState({ error: 'Senhas não conferem*' });
      return;
    }

    this.setState({ error: '' });

    if (isEmail({ email })) {
      /*
      
        BACKEND REGISTER NEW USER
      
      */

      try {
        const res = await api.post('/api/user/create-user', {
          ...this.state,
        });

        // Check if it was successful
        if (!res.data.success) {
          alert('Cadastro falhou! Tente novamente.');
          return;
        }

        // Navigate to the home screen
        this.props.navigation.navigate('TempPage');

        console.log(res.data);
      } catch (error) {
        alert('Erro interno, tente novamente mais tarde.');
      }
    } else {
      this.setState({ error: 'Email inválido*' });
      return;
    }

    alert('Cadastro realizado com sucesso!');
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.rootContainer}>
        {/* Banner */}
        <View style={styles.bannerContainer}>
          <Image
            source={require('../../../assets/images/logo-transparent.png')}
            resizeMode="contain"
            style={styles.banner}
          />
        </View>
        {/* Form */}
        <View style={styles.formContainer}>
          <AuthInput
            type="user"
            placeholder={'Nome de Usuário'}
            value={this.state.fullname}
            onChangeText={(value) => this.setState({ fullname: value })}
          />
          <AuthInput
            type="envelope"
            placeholder={'Email'}
            value={this.state.email}
            onChangeText={(value) => this.setState({ email: value })}
          />
          <AuthInput
            type="lock"
            placeholder={'Senha'}
            value={this.state.password}
            onChangeText={(value) => this.setState({ password: value })}
            secureTextEntry={true}
          />
          <AuthInput
            type="lock"
            placeholder={'Confirme sua senha'}
            value={this.state.confirmPassword}
            onChangeText={(value) => this.setState({ confirmPassword: value })}
            secureTextEntry={true}
          />
          <Text style={styles.error}>{this.state.error}</Text>
        </View>
        <View style={styles.buttonRow}>
          <AuthButton
            text="Cadastrar"
            type="primary"
            onPress={() => {
              this.register();
            }}
          />
          <AuthButton
            text="Voltar"
            type="secondary"
            onPress={() => {
              this.props.navigation.goBack();
            }}
          />
        </View>
      </View>
    );
  }
}
