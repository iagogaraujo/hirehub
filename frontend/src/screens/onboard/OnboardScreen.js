import React, { Component } from 'react';
import { Image, StyleSheet } from 'react-native';
import Onboarding from 'react-native-onboarding-swiper';

export class OnboardScreen extends Component {
  render() {
    return (
      <Onboarding
        onSkip={() => this.props.navigation.navigate('Introduction')}
        onDone={() => this.props.navigation.navigate('Introduction')}
        skipLabel="Pular"
        nextLabel="Próximo"
        bottomBarColor="#fff"
        controlStatusBar={false}
        pages={[
          {
            backgroundColor: '#fff',
            bottomBarColor: '#fff',
            image: (
              <Image
                source={require('../../assets/onboard/onboard1.png')}
                style={{ width: 300, height: 300 }}
              />
            ),
            title: 'Bem vindo!',
            subtitle: 'Seja bem vindo a nossa plataforma gratuita!',
          },
          {
            backgroundColor: '#fff',
            image: (
              <Image
                source={require('../../assets/onboard/onboard2.png')}
                style={{ width: 300, height: 300 }}
              />
            ),
            title: 'Encontre Serviços!',
            subtitle:
              'Encontre dos mais variados serviços em nossa plataforma!',
          },
          {
            backgroundColor: '#fff',
            image: (
              <Image
                source={require('../../assets/onboard/onboard3.png')}
                style={{ width: 300, height: 300 }}
              />
            ),
            title: 'Ofereça Seus Serviços!',
            subtitle: 'Crie e esponha seus serviços com maior facilidade!',
          },
          {
            backgroundColor: '#fff',
            image: (
              <Image
                source={require('../../assets/onboard/onboard4.png')}
                style={{ width: 300, height: 300 }}
              />
            ),
            title: 'Crie ofertas!',
            subtitle:
              'Não encontrou o que desejava? Não entre em pânico! Crie uma oferta para pessoas interessadas!',
          },
        ]}
      />
      // <View>
      //   <Text> Hello, World! </Text>
      // </View>
    );
  }
}

export default OnboardScreen;
