export default {
  PRIMARY_COLOR: '#505050',
  SECONDARY_COLOR: '#FDFDFD',
  TERTIARY_COLOR: '#CFCDD8',
  QUATERNARY_COLOR: '#ECECEA',
  QUINARY_COLOR: '#E3E3E1',
};
