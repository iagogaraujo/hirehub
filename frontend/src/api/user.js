import AsyncStorage from '@react-native-async-storage/async-storage';
// API URL
import api from './helper';

// Essa função é responsável por pegar os dados do usuário
export const getUserData = async () => {
  const token = await AsyncStorage.getItem('token');

  api.defaults.headers.common['Authorization'] = `Bearer ${token}`;

  const res = await api.post('/api/user/get-user-data');
  const data = res.data.user;

  return { ...data };
};
