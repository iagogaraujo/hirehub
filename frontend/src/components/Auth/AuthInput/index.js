import { Text, StyleSheet, View, TextInput } from 'react-native';
import React, { Component } from 'react';
import styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';

const AuthInput = ({
  onPress,
  value,
  placeholder,
  type,
  onChangeText,
  secureTextEntry,
}) => {
  return (
    <View style={styles.container}>
      <Icon name={type} size={20} color="#000" style={styles.inputIcon} />
      <TextInput
        placeholder={placeholder}
        style={styles.input}
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
      />
    </View>
  );
};

export default AuthInput;
