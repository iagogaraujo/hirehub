import { StyleSheet } from 'react-native';
import colors from '../../../assets/styles';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.QUATERNARY_COLOR,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    height: 45,
    marginBottom: 10,
    paddingHorizontal: 10,
    width: '80%',
  },
  input: {
    // paddingHorizontal: '100%',
    width: '100%',
    left: 40,
    // flex: 1,
    // fontSize: 16,
    // color: '#000',
  },

  inputIcon: {
    position: 'absolute',
    // top: 0,
    left: 10,
  },
});
