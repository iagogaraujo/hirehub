import { StyleSheet } from 'react-native';
import colors from '../../../assets/styles';

export default StyleSheet.create({
  button: {
    borderRadius: 5,
    alignItems: 'center',
    elevation: 3,
  },
  buttonText: {
    // color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    paddingHorizontal: 70,
    paddingVertical: 8,
  },
  button_primary: {
    backgroundColor: colors.PRIMARY_COLOR,
  },
  button_secondary: {
    backgroundColor: colors.QUATERNARY_COLOR,
  },
  buttonText_primary: {
    color: colors.QUATERNARY_COLOR,
  },
  buttonText_secondary: {
    color: colors.PRIMARY_COLOR,
  },
  size_small: {
    fontSize: 16,
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
});
