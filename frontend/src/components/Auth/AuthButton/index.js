import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Vibration,
} from 'react-native';
import React, { Component } from 'react';
import styles from './styles';

const handlePress = ({ onPress }) => {
  Vibration.vibrate(100);
  if (!onPress) return;
  onPress();
};

const AuthButton = ({ onPress, text, type, size }) => {
  return (
    <TouchableOpacity
      style={[styles.button, styles[`button_${type}`]]}
      onPress={() => handlePress({ onPress })}
    >
      <Text
        style={[
          styles.buttonText,
          styles[`buttonText_${type}`],
          styles[`size_${size}`],
        ]}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};

export default AuthButton;
