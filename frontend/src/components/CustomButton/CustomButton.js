import React from 'react'
import { View, Text, TextInput, StyleSheet, Pressable, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const CustomButtom = ({onPress, text, type}) => {
  return (
    <Pressable style={[styles.container, styles[`container_${type}`]]} onPress={onPress}>
      <Text style={styles.text}>{text}</Text>
    </Pressable>
  )
}

const styles = StyleSheet.create({
  view: {
    fontFamily: 'Roboto',
    fontSize: 50,
  },

  container: {
    // backgroundColor:'#3B71F3',
    width: '100%',

    borderColor: '#e8e8e8',
    borderWidth: 1,
    borderRadius: 5,

    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 5,

    alignItems: 'center',
    alignContent: 'space-around',

  },

  container_PRIMARY: {
    backgroundColor:'#3B71F3',
  },

  container_SECONDARY: {
    backgroundColor:'#5A6268',
  },

  container_GOOGLE: {
    backgroundColor:'#C82333',
  },

  text: {
    color: 'white',
    fontWeight: 'bold',
  },

  icon: {
    width: '100%',
    height: 230,
  }
})

export default CustomButtom