import { StyleSheet } from 'react-native';
import {
  VStack,
  HStack,
  Button,
  IconButton,
  Icon,
  Text,
  NativeBaseProvider,
  Center,
  Box,
  StatusBar,
  View,
} from 'native-base';
import { MaterialIcons } from '@expo/vector-icons';
import React from 'react';

import colors from '../../assets/styles';

const CustomAppBar = ({ text }) => {
  return (
    <Box bg={colors.SECONDARY_COLOR} shadow="5">
      <StatusBar bg={colors.PRIMARY_COLOR} barStyle="light-content" />
      <Box safeAreaTop bg={colors.SECONDARY_COLOR} />
      <HStack
        bg={colors.SECONDARY_COLOR}
        px="1"
        py="4"
        justifyContent="space-between"
        alignItems="center"
        w="100%"
      >
        <HStack alignItems="center">
          <IconButton
            icon={
              <Icon
                size="sm"
                as={MaterialIcons}
                name="menu"
                color={colors.PRIMARY_COLOR}
              />
            }
          />
          <Text color={colors.PRIMARY_COLOR} fontSize="20" fontWeight="bold">
            {text}
          </Text>
        </HStack>
        <HStack>
          <IconButton
            icon={
              <Icon
                as={MaterialIcons}
                name="search"
                size="sm"
                color={colors.PRIMARY_COLOR}
              />
            }
          />
          <IconButton
            icon={
              <Icon
                as={MaterialIcons}
                name="more-vert"
                size="sm"
                color={colors.PRIMARY_COLOR}
              />
            }
          />
        </HStack>
      </HStack>
    </Box>
  );
};

export default CustomAppBar;

const styles = StyleSheet.create({});
