import { StyleSheet, TouchableOpacity, View } from 'react-native';
import React, { Component } from 'react';
import {
  NativeBaseProvider,
  Box,
  Text,
  Heading,
  VStack,
  FormControl,
  Input,
  Link,
  Button,
  Icon,
  HStack,
  Center,
  Pressable,
  IconButton,
} from 'native-base';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import colors from '../../assets/styles/index';

export default class CustomFooter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      /*
        Buttons: Home, Search, Add, Notifications, Profile
      */
      <Box
        safeAreaBottom
        bg={colors.SECONDARY_COLOR}
        flexDirection="row"
        justifyContent="space-around"
        alignItems="center"
        px={2}
        py={2}
        position="absolute"
        bottom={0}
        left={0}
        right={0}
        borderTopLeftRadius={10}
        borderTopRightRadius={10}
        styles={[styles.boxShadow]}
      >
        <TouchableOpacity
          style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center'}}
          onPress={() => this.props.navigation.navigate('Home')}
          // onPress={() => console.log('Home')}
        >
          <MaterialCommunityIcons name="home" size={28} color={colors.white} />
        </TouchableOpacity>
        <TouchableOpacity
        style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center'}}
          onPress={() => this.props.navigation.navigate('Search')}
          // onPress={() => console.log('Search')}
        >
          <MaterialCommunityIcons
            name="magnify"
            size={28}
            color={colors.white}
          />
        </TouchableOpacity>
        <TouchableOpacity
        style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center'}}
          // onPress={() => this.props.navigation.navigate('Add')}
          onPress={() => this.props.navigation.navigate('Add')}
        >
          <MaterialCommunityIcons name="plus" size={28} color={colors.white} />
        </TouchableOpacity>
        <TouchableOpacity
        style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center'}}
          // onPress={() => this.props.navigation.navigate('Notifications')}
          onPress={() => console.log('Notifications')}
        >
          <MaterialCommunityIcons name="bell" size={28} color={colors.white} />
        </TouchableOpacity>
        <TouchableOpacity
        style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center'}}
          onPress={() => this.props.navigation.navigate('Profile')}
          // onPress={() => console.log('Profile')}
        >
          <MaterialCommunityIcons
            name="account"
            size={28}
            color={colors.white}
          />
        </TouchableOpacity>
      </Box>
    );
  }
}

const styles = StyleSheet.create({
  boxShadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
