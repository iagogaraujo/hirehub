import { Text, StyleSheet, View } from 'react-native'
import React, { Component } from 'react'

export default function OnboardItem ({item}) {
  return(
      <View style={styles.container}>
        <View style={styles.mockImage} />
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description}>{item.description}</Text>
      </View>
  )
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
  },

  mockImage: {
    backgroundColor: '#a1a1a1',
    height: 200,
    width: 250,
    borderRadius: 16,
    margin: 16
  },

  title: {
    fontWeight: '800',
    fontSize: 28,
    marginBottom: 10,
    color: '#493d8a',
    textAlign: 'center',
  },
  description: {
    fontWeight: 400,
    color: '#62656b',
    textAlign: 'center',
    paddingHorizontal: 64,

  },

})