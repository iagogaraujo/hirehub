export default [
  {
    id: '1',
    title: 'Seja bem vindo!',
    description: 'Bem vindo a uma nova forma de inovar a sua forma de empreender!',
    image: require('../../assets/undraw_Dreamer_re_9tua.png'),
  },
  {
    id: '2',
    title: 'Encontre Serviços!',
    description: 'Encontre dos mais variados serviços em nossa plataforma!',
    image: require('../../assets/undraw_People_search_re_5rre.png'),
  },
  {
    id: '3',
    title: 'Ofereça Seus Serviços!',
    description: 'Crie e esponha seus serviços com maior facilidade!',
    image: require('../../assets/undraw_Business_deal_re_up4u.png'),
  },
  {
    id: '4',
    title: 'Crie ofertas!',
    description: 'Não encontrou o que desejava? Não entre em pânico! Crie uma oferta para pessoas interessadas!',
    image: require('../../assets/undraw_Blog_post_re_fy5x.png')
  },
]