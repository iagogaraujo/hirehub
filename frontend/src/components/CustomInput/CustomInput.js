import React from 'react'
import { View, Text, TextInput, StyleSheet, Image } from 'react-native'

const CustomInput = ({value, setValue, placeholder, secureTextEntry}) => {
  return (
    <View style={styles.container}>
      {/*<Image source={require('../../assets/person-circle.svg')} style={styles.ImageStyle}/>*/}

      <TextInput 
      value={value}
      onChanceText={setValue}
      style={styles.input}
      placeholder={placeholder}
      secureTextEntry={secureTextEntry}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: '100%',

    borderColor: '#e8e8e8',
    borderWidth: 1,
    borderRadius: 5,

    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 5,

  },  
  input: {},
  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode : 'stretch',
    alignItems: 'center'
  }
})

export default CustomInput