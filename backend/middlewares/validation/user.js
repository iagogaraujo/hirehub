const { check, validationResult } = require('express-validator');

exports.validateUserSignUp = [
  check('fullname')
    .trim()
    .not()
    .isEmpty()
    .withMessage('Nome completo é obrigatório')
    .isString()
    .withMessage('Nome completo deve válido')
    .isLength({ min: 3, max: 20 })
    .withMessage('Nome completo deve ter entre 3 e 20 caracteres'),
  check('email').normalizeEmail().isEmail().withMessage('Email inválido'),
  check('password')
    .trim()
    .not()
    .isEmpty()
    .withMessage('Senha não pode ser vazia')
    .isLength({ min: 3, max: 20 })
    .withMessage('Senha deve ter entre 3 e 20 caracteres'),
  check('confirmPassword')
    .trim()
    .not()
    .isEmpty()
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error('Senhas não conferem');
      }
      return true;
    }),
];

exports.userValidation = (req, res, next) => {
  const result = validationResult(req).array();
  // console.log(result);
  if (!result.length) return next();

  const error = result[0].msg;
  res.status(400).json({ success: false, message: error });
};

exports.validateUserSignIn = [
  check('email').trim().isEmail().withMessage('Email inválido'),
  check('password')
    .trim()
    .not()
    .isEmpty()
    .withMessage('Senha não pode ser vazia'),
];
