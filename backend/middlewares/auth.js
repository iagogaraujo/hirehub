const jwt = require('jsonwebtoken');
const User = require('../models/user');

/*

  Authentification middleware

*/

exports.isAuth = async (req, res, next) => {
  console.log(' === Requisição recebida ===');
  console.log(' => Token: ' + req.headers['authorization']);

  // Verifica se o token existe
  if (req.headers && req.headers.authorization) {
    const token = req.headers.authorization.split(' ')[1];

    try {
      // Decodifica o token
      const decode = jwt.verify(token, process.env.JWT_SECRET);
      const user = await User.findById(decode.userId);

      // Verifica se o usuário existe
      if (!user) {
        return res.json({ success: false, message: 'Acesso não autorizado!' });
      }

      req.user = user;

      next();
    } catch (error) {
      // Caso o token seja inválido
      if (error.name === 'JsonWebTokenError') {
        return res.json({ success: false, message: 'Acesso não autorizado!' });
      }
      // Caso o token tenha expirado
      if (error.name === 'TokenExpiredError') {
        return res.json({ success: false, message: 'Sessão expirou!' });
      }
      // Erro interno
      res.json({ success: false, message: 'Erro interno!' });
    }
  } else {
    // Se o token não existir
    res.json({ success: false, message: 'Acesso não autorizado!' });
  }
};
