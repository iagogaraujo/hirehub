const express = require('express');
require('dotenv').config();
require('./models/db');

const userRouter = require('./routes/user');
const serviceRouter = require('./routes/service');
const User = require('./models/user');
const Service = require('./models/service');

const app = express();
const port = 3000;

app.use(express.json());
app.use('/api/user', userRouter);
app.use('/api/service', serviceRouter);

app.use('*', (req, res) => {
  res.status(404).json({
    success: false,
    error: 'Rota não encontrada',
  });
});

const test = async (email, password) => {
  const user = await User.findOne({ email: email });
  const result = await user.comparePasswords(password);
  console.log(result);
};

app.listen(port, () => console.log(`Servidor iniciado na porta: ${port}!`));
