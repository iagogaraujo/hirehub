const jwt = require('jsonwebtoken');
const User = require('../models/user');

/*

  CONTROLE DE USUÁRIOS - BACKEND

*/

/* CRIAÇÃO DE USUÁRIOS */
exports.createUser = async (req, res) => {
  const { fullname, email, password, usertype } = req.body;
  const isNewUser = await User.isThisEmailInUse(email);

  if (!isNewUser)
    return res.json({
      success: false,
      message: 'Email já cadastrado!',
    });

  const user = await User({
    fullname,
    email,
    password,
    usertype,
  });
  await user.save();

  res.json({
    success: true,
    message: 'Usuário criado com sucesso!',
    user,
  });
};

/* CONTROLE DE AUTENTICAÇÃO */
exports.userSignIn = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });

  if (!user)
    return res
      .status(401)
      .json({ success: false, message: 'Email / Senha inválidos' });

  const isMatch = await user.comparePasswords(password);
  if (!isMatch)
    return res
      .status(401)
      .json({ success: false, message: 'Email / Senha inválidos' });

  const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, {
    expiresIn: '1d',
  });

  res.status(200).json({ success: true, user, token });
};

/* ATUALIZAÇÃO DE FOTO DE PERFIL */
exports.updateUserProfile = async (req, res) => {
  const user = req.user;
  if (!user)
    return res.status(401).json({ success: false, message: 'Não autorizado!' });

  try {
    const profileBuffer = req.file.buffer;

    const { width, height } = await sharp(profileBuffer).metadata();
    const avatar = await sharp(profileBuffer)
      .resize(Math.round(width * 0.5), Math.round(height * 0.5))
      .toBuffer();

    User.findByIdAndUpdate(user._id, { avatar });
    res.status(201).json({ success: true, message: 'Imagem atualizada!' });
  } catch (error) {
    console.log('Error: ', error);
    res.status(500).json({ success: false, message: 'Erro interno!' });
  }
};

/* OBTENÇÃO DE DADOS DO USUÁRIO */
exports.getUserData = async (req, res) => {
  const token = req.headers.authorization.split(' ')[1];
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findById(decoded.userId);
    if (!user)
      return res.status(404).json({
        success: false,
        message: 'Usuário não encontrado!',
      });
    res.status(200).json({ success: true, user });
  } catch (error) {
    console.log('Erro ao obter dados de usuário');
    console.log(error);
    return null;
  }
};
