const Service = require('../models/service');

exports.getService = async (req, res) => {
  const services = await Service.find();
  res.status(200).json({ success: true, services });
};

exports.getServiceByName = async (req, res) => {
  console.log('Requisição recebida: ' + req.body)
  // Verifica se o nome foi passado
  const { name } = req.body;
  if (!name) {
    // console.log("Nome não existe!!!")
    return res.status(400).json({ success: false, message: "Nome faltando!" });
  }
  // console.log("Nome: " + name   )

  // Verifica se serviços que iniciam com o nome foram encontrados
  const services = await Service.find({ name: { $regex: '^' + name } });
  if (services.length == 0) {
    // console.log("Nenhum serviço encontrado!!!")
    return res.status(404).json({ success: false, message: "Nenhum serviço encontrado!" });
  }
  // console.log("Serviços: " + services)

  // Retorna os serviços encontrados
  res.status(200).json({ success: true, services });
};

exports.createService = async (req, res) => {
  const necessaryOptions = [
    'name',
    'description',
    'price',
    'image',
    'category',
    'location',
  ];

  // Verifica se todos os campos necessários foram preenchidos
  for (var i = 0; i < necessaryOptions.length; i++) {
    console.log(
      'Verificando se o argumento ' + necessaryOptions[i] + ' existe...'
    );
    if (!req.body[necessaryOptions[i]]) {
      console.log('O argumento ' + necessaryOptions[i] + ' não existe');
      return res.status(400).json({
        success: false,
        error: 'É necessário o uso de todos os argumentos',
      });
    }
    console.log('...OK');
  }

  const { name, description, price, image, category, user, location } =
    req.body;

  try {
    const service = await Service({
      name,
      description,
      price,
      image,
      category,
      user,
      location,
    });
    await service.save();
    console.log(' ---> Serviço Criado');
    res.json({ success: true, service });
  } catch (error) {
    res.json({ success: false, error });
  }
};
