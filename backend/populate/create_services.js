const axios = require('axios');

const createService = (name, description, price, image, category, location) => {
  axios.post('http://localhost:3000/create-service', {
    name,
    description,
    price,
    image,
    category,
    location,
  });
};

createService(
  'Corte de Cabelo',
  'Corte de cabelo masculino',
  20,
  'https://images.unsplash.com/photo-1503951914875-452162b0f3f1?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
  'Cabelo',
  'São Paulo'
);
createService(
  'Pintura',
  'Pintura de parede',
  50,
  'https://images.unsplash.com/photo-1562259949-e8e7689d7828?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1131&q=80',
  'Pintura',
  'São Paulo'
);
createService(
  'Mecânica',
  'Troca de óleo',
  100,
  'https://images.unsplash.com/photo-1619642751034-765dfdf7c58e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80',
  'Mecânica',
  'São Paulo'
);
createService(
  'Limpeza',
  'Limpeza de domicílios',
  50,
  'https://images.unsplash.com/photo-1502005097973-6a7082348e28?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80',
  'Limpeza',
  'São Paulo'
);
