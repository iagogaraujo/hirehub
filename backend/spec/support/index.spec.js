var Request = require('request');
const axios = require('axios');

describe('API - Services', () => {
  var server;
  beforeAll(() => {
    server = require('../../index');
  });
  afterAll(() => {
    server.close();
  });
  describe('GET /api/service/get-all', () => {
    var data = {};
    beforeAll((done) => {
      Request.get(
        'http://localhost:3000/api/service/get-all',
        (error, response, body) => {
          data.status = response.statusCode;
          data.body = body;
          done();
        }
      );
    });
    it('Status 200', () => {
      expect(data.status).toBe(200);
    });
    it('Corpo da requisição', () => {
      expect(data.body).toBeTruthy();
    });
  });

  describe('POST /api/service/create-service - Sem Argumentos', () => {
    var data = {};
    var jsonDataObj = {};
    beforeAll((done) => {
      Request.post(
        {
          headers: { 'content-type': 'application/x-www-form-urlencoded' },
          url: 'http://localhost:3000/api/service/create-service',
          json: { msg: '' },
        },
        (error, response, body) => {
          data.status = response.statusCode;
          data.body = body;
          done();
        }
      );
    });
    it('Status', () => {
      expect(data.status).toBe(400);
    });
    it('Corpo da requisição', () => {
      expect(data.body['error']).toEqual(
        'É necessário o uso de todos os argumentos'
      );
    });
  });

  describe('GET /ERROR', () => {
    var data = {};
    beforeAll((done) => {
      Request.get('http://localhost:3000/ERROR', (error, response, body) => {
        data.status = response.statusCode;
        data.body = body;
        done();
      });
    });
    // If the route is not found
    it('Status 404', () => {
      expect(data.status).toBe(404);
    });
    it('Error in Body', () => {
      expect(data.body).toContain('Rota não encontrada');
    });
  });
});

describe('API - Users', () => {
  var server;
  beforeAll(() => {
    server = require('../../index');
  });
  afterAll(() => {
    server.close();
  });
  describe('POST /api/user/sign-in', () => {
    var data = {};
    beforeAll((done) => {
      // Request.post(
      //   {
      //     headers: { 'content-type': 'application/x-www-form-urlencoded' },
      //     url: 'http://localhost:3000/api/user/sign-in',
      //     json: { email: 'tester@email.com', password: '123' },
      //   },
      //   (error, response, body) => {
      //     data.status = response.statusCode;
      //     data.body = body;
      //     done();
      //   }
      // );
      axios
        .post('http://127.0.0.1:3000/api/user/sign-in', {
          email: 'tester@email.com',
          password: '123',
        })
        .then((response) => {
          data.status = response.status;
          data.body = response.data;
          done();
        });
    });
    it('Status 200', () => {
      expect(data.status).toBe(200);
    });
    it('Corpo da requisição', () => {
      expect(data.body['success']).toEqual(true);
    });
  });

  describe('POST /api/user/sign-in - Login inválido!!!', () => {
    var data = {};
    beforeAll((done) => {
      axios
        .post('http://127.0.0.1:3000/api/user/sign-in', {
          email: 'tester@email.com',
          password: '1234',
        })
        .then((response) => {
          data.status = response.status;
          data.body = response.data;
          done();
        })
        .catch((error) => {
          data.status = error.response.status;
          data.body = error.response.data;
          done();
        });
    });
    it('Status 401', () => {
      expect(data.status).toBe(401);
    });
    it('Corpo da requisição', () => {
      expect(data.body['success']).toEqual(false);
    });
  });

  describe('POST /api/user/sign-in - Argumentos inválidos!!!', () => {
    var data = {};
    beforeAll((done) => {
      axios
        .post('http://127.0.0.1:3000/api/user/sign-in', {
          email: 'testeremail.com',
          password: '123',
        })
        .then((response) => {
          data.status = response.status;
          data.body = response.data;
          done();
        })
        .catch((error) => {
          data.status = error.response.status;
          data.body = error.response.data;
          done();
        });
    });
    it('Status 400', () => {
      expect(data.status).toBe(400);
    });
  });
});
