//
// Esse arquivo é responsável pelo gerenciamento das rotas de serviços
//

const express = require('express');

const router = express.Router();
const { getService, createService, getServiceByName } = require('../controllers/service');

// @endpoint: /api/service
// @method: GET - Retorna todos os serviços
router.get('/get-all', getService);

// @method: POST - Retorna todos os serviços com o nome especificado
router.post('/get-by-name', getServiceByName);

// @method: POST - Cria um novo serviço
router.post('/create-service', createService);

// Exporta o router
module.exports = router;
