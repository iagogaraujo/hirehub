//
// Esse arquivo é responsável pelo gerenciamento das rotas de usuários
//

const express = require('express');
const router = express.Router();
const {
  createUser,
  userSignIn,
  updateUserProfile,
  getUserData,
} = require('../controllers/user');
const { isAuth } = require('../middlewares/auth');
const {
  validateUserSignUp,
  userValidation,
  validateUserSignIn,
} = require('../middlewares/validation/user');
const multer = require('multer');
const storage = multer.memoryStorage();

// Função para salvar a imagem no banco de dados
const fileFilter = (req, file, cb) => {
  if (file.mimetype.startsWith('image')) {
    cb(null, true);
  } else {
    cb(new Error('Não é uma imagem!.', 400), false);
  }
};

// Configurações do multer
// Storage usada vai ser a memória
const uploads = multer({ storage, fileFilter });

// JWT para autenticação
const jwt = require('jsonwebtoken');

// @endpoint: /api/user
// @method POST - Cria um novo usuário
router.post('/create-user', validateUserSignUp, userValidation, createUser);
// @method POST - Faz o login do usuário
router.post('/sign-in', validateUserSignIn, userValidation, userSignIn);
// @method POST - Atualiza a foto de perfil do usuário
router.post(
  '/upload-profile',
  isAuth,
  uploads.single('profile'),
  updateUserProfile
);

// @method POST - Obtem dados do usuário a partir do token
router.post('/get-user-data', isAuth, getUserData);

module.exports = router;
